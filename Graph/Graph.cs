﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Graph {
    public interface IGraph<T> {
        IEnumerable<IEnumerable<T>> RoutesBetween(T source, T target);
    }

    /// <summary>
    /// This class represents a Graph.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Graph<T> : IGraph<T> {
        /// <summary>
        /// Our dictionary of links.
        /// </summary>
        private Dictionary<T, List<T>> links;
        /// <summary>
        /// Our dictionary of links. Key: T. Value: List of Ts.
        /// </summary>
        public Dictionary<T, List<T>> Links { get => links; set => links = value; }

        /// <summary>
        /// Constructor. Receives links to save on Links dictionary
        /// </summary>
        /// <param name="links"></param>
        public Graph(IEnumerable<ILink<T>> links) { 
            // Initialize dictionary
            Links = new Dictionary<T, List<T>>();

            // Populate dictionary
            foreach(var l in links) {
                // Source is key, value is target. Like a link!
                T key = l.Source;
                T value = l.Target;

                // If we have a key, we just add the value.
                if(Links.ContainsKey(key)) {
                    List<T> targets = Links[key];

                    if(!targets.Contains(value)) {
                        targets.Add(value);
                    }
                }
                // Otherwise, add new key.
                else {
                    List<T> targets = new List<T>();
                    targets.Add(value);
                    Links.Add(key, targets);
                }
            }
        }

        /// <summary>
        /// Returns all routes between source and target.
        /// </summary>
        /// <param name="source">From.</param>
        /// <param name="target">To.</param>
        /// <returns></returns>
        public IEnumerable<IEnumerable<T>> RoutesBetween(T source, T target) {
            // These lists are used to save previous gathered info.
            // Path = Our current path.
            var path = new List<T>();
            // List of finished paths.
            var results = new List<List<T>>();
            // T's we visited. Avoids repeating.
            var visited = new List<T>();

            // Add first element and search the rest.
            path.Add(source);
            SearchAllPaths(results, visited, path, source, target, 100);

            return results;
        }

        /// <summary>
        /// Searches all paths from source to target.
        /// </summary>
        /// <param name="results">List of finished paths.</param>
        /// <param name="visited">T's we already visited. Avoids repeating.</param>
        /// <param name="path">Our current path.</param>
        /// <param name="source">From.</param>
        /// <param name="target">To.</param>
        /// <returns></returns>
        bool SearchAllPaths(List<List<T>> results, List<T> visited, List<T> path, T source, T target, int maximumIterations) {
            // We visited source now, so we save it.
            visited.Add(source);

            // If we have nowhere to go, return false.
            if(!Links.ContainsKey(source))
                return false;

            // See if we can go directly to target.
            if(Links[source].Any(x => x.Equals(target))) {
                // We can. Let's add target to path.
                path.Add(target);
                // Save path to results and we are done.
                results.Add(path);
                return true;
            }

            // We have this here to avoid stack overflow.
            if(maximumIterations-- < 0)
                return false;

            // Search for each of our targets.
            foreach(var route in Links[source]) {
                // Avoid moving in circles, ignoring each visited target.
                if(visited.Any(x => x.Equals(route)))
                    continue;

                // Clone routes to avoid messing our path.
                var thisRoutes = new List<T>(path);
                thisRoutes.Add(route);

                // Now we just to everything over and over again until stackoverflow or success
                SearchAllPaths(results, visited, thisRoutes, route, target, maximumIterations);
            }
            
            // No path, return false.
            return false;
        }

    }
}
