# README #

This is my take on a evaluation test for developer position.

These were the 2 tests:

- 1: Imagine that we have a subtitle file (.srt) out of sync. The program must be able to shift the whole source file by the given timespan.
- 2: Given a predefined list of links, the program must be able to calculate the possible paths between one point and another.

The first one was solved with a regular expression to match the subtitle's timeSpan, format it and add the difference.

The second used a recursive approach to search for every route and return all the possible paths between source and target.

Marcelo Ruberto Junior, 01/08/2019.