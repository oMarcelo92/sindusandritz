﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SubtitleTimeshift
{
    /// <summary>
    /// This class is responsible for shifting the timeSpan of the received subtitles file.
    /// </summary>
    public class Shifter
    {
        /// <summary>
        /// This async method takes a Stream input, adds timeSpan to synchronize it, and saves it on an output stream.
        /// </summary>
        /// <returns></returns>
        async static public Task Shift(Stream input, Stream output, TimeSpan timeSpan, Encoding encoding, int bufferSize = 1024, bool leaveOpen = false)
        {
            // This is the default subtitle timeSpan pattern.
            // \d{1,2} gets 1 or 2 characters, it is needed to work when the subtitles's timespans have or don't have 0 to the left. (eg 1 or 01)
            Regex r = new Regex(@"(\d{1,2}):(\d{1,2}):(\d{1,2}).(\d{3})");

            // Convert stream to string
            StreamReader reader = new StreamReader(input);
            string inputText = reader.ReadToEnd();

            // Replace the old timespan with the new one and save on a string
            string newText = r.Replace(inputText, m => ShiftTime(m, timeSpan));

            // Convert string to stream
            output.Position = 0;
            StreamWriter writer = new StreamWriter(output, encoding, bufferSize, leaveOpen);

            // Write the new string on the output file
            await writer.WriteAsync(newText);
            writer.Flush();
        }

        /// <summary>
        /// Adds timeDifference to the current match string.
        /// </summary>
        /// <returns></returns>
        static string ShiftTime(Match match, TimeSpan timeDifference)
        {
            TimeSpan newTime = TimeSpan.ParseExact(match.Value, @"h%\:m%\:s%\,fff", CultureInfo.InvariantCulture);            

            // Adds and return.
            return newTime.Add(timeDifference).ToString(@"hh\:mm\:ss\.fff");
        }
    }
}
